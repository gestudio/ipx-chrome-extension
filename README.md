### Yet Another Flags source code ###

This is the source code for Google Chrome [extenstion](https://chrome.google.com/webstore/detail/dmchcmgddbhmbkakammmklpoonoiiomk).

Extension takes data from free [MaxMind City DB](http://www.maxmind.com/app/geolitecity)
files. Data is then served by
nodejs application that uses [bogart](https://github.com/nrstott/bogart)
and excellent [geoip](https://github.com/kuno).

Take a look at [server code](https://github.com/falsefalse/geoip-server) as well.

